#include "MFcamera.h"

#include <strsafe.h>

LPCWSTR GetGUIDNameConst(const GUID& guid);
HRESULT GetGUIDName(const GUID& guid, WCHAR **ppwsz);

HRESULT LogAttributeValueByIndex(IMFAttributes *pAttr, DWORD index);
HRESULT SpecialCaseAttributeValue(GUID guid, const PROPVARIANT& var);

void DBGMSG(PCWSTR format, ...);

HRESULT LogMediaType(IMFMediaType *pType)
{
	UINT32 count = 0;
	if (pType == NULL)
		return MF_E_INVALIDMEDIATYPE;
	HRESULT hr = pType->GetCount(&count);
	if (FAILED(hr))
	{
		return hr;
	}

	if (count == 0)
	{
		DBGMSG(L"Empty media type.\n");
	}
	printf("Media type count %d\n", count);
	for (UINT32 i = 0; i < count; i++)
	{
		hr = LogAttributeValueByIndex(pType, i);
		if (FAILED(hr))
		{
			break;
		}
	}
	return hr;
}

HRESULT LogAttributeValueByIndex(IMFAttributes *pAttr, DWORD index)
{
	WCHAR *pGuidName = NULL;
	WCHAR *pGuidValName = NULL;

	GUID guid = { 0 };

	PROPVARIANT var;
	PropVariantInit(&var);

	HRESULT hr = pAttr->GetItemByIndex(index, &guid, &var);

	if (FAILED(hr))
	{
		goto done;
	}
	hr = GetGUIDName(guid, &pGuidName);
	if (FAILED(hr))
	{
		goto done;
	}

	DBGMSG(L"\t%s\t", pGuidName);

	hr = SpecialCaseAttributeValue(guid, var);
	if (FAILED(hr))
	{
		goto done;
	}
	if (hr == S_FALSE)
	{
		switch (var.vt)
		{
		case VT_UI4:
			DBGMSG(L"%d", var.ulVal);
			break;

		case VT_UI8:
			DBGMSG(L"%I64d", var.uhVal);
			break;

		case VT_R8:
			DBGMSG(L"%f", var.dblVal);
			break;

		case VT_CLSID:
			hr = GetGUIDName(*var.puuid, &pGuidValName);
			if (SUCCEEDED(hr))
			{
				DBGMSG(pGuidValName);
			}
			break;

		case VT_LPWSTR:
			DBGMSG(var.pwszVal);
			break;

		case VT_VECTOR | VT_UI1:
			DBGMSG(L"<<byte array>>");
			break;

		case VT_UNKNOWN:
			DBGMSG(L"IUnknown");
			break;

		default:
			DBGMSG(L"Unexpected attribute type (vt = %d)", var.vt);
			break;
		}
	}

done:
	DBGMSG(L"\n");
	CoTaskMemFree(pGuidName);
	CoTaskMemFree(pGuidValName);
	PropVariantClear(&var);
	return hr;
}

HRESULT GetGUIDName(const GUID& guid, WCHAR **ppwsz)
{
	HRESULT hr = S_OK;
	WCHAR *pName = NULL;

	LPCWSTR pcwsz = GetGUIDNameConst(guid);
	if (pcwsz)
	{
		size_t cchLength = 0;

		hr = StringCchLengthW(pcwsz, STRSAFE_MAX_CCH, &cchLength);
		if (FAILED(hr))
		{
			goto done;
		}

		pName = (WCHAR*)CoTaskMemAlloc((cchLength + 1) * sizeof(WCHAR));

		if (pName == NULL)
		{
			hr = E_OUTOFMEMORY;
			goto done;
		}

		hr = StringCchCopyW(pName, cchLength + 1, pcwsz);
		if (FAILED(hr))
		{
			goto done;
		}
	}
	else
	{
		hr = StringFromCLSID(guid, &pName);
	}

done:
	if (FAILED(hr))
	{
		*ppwsz = NULL;
		CoTaskMemFree(pName);
	}
	else
	{
		*ppwsz = pName;
	}
	return hr;
}

void LogUINT32AsUINT64(const PROPVARIANT& var)
{
	UINT32 uHigh = 0, uLow = 0;
	Unpack2UINT32AsUINT64(var.uhVal.QuadPart, &uHigh, &uLow);
	DBGMSG(L"%d x %d", uHigh, uLow);
}

float OffsetToFloat(const MFOffset& offset)
{
	return offset.value + (static_cast<float>(offset.fract) / 65536.0f);
}

HRESULT LogVideoArea(const PROPVARIANT& var)
{
	if (var.caub.cElems < sizeof(MFVideoArea))
	{
		return MF_E_BUFFERTOOSMALL;
	}

	MFVideoArea *pArea = (MFVideoArea*)var.caub.pElems;

	DBGMSG(L"(%f,%f) (%d,%d)", OffsetToFloat(pArea->OffsetX), OffsetToFloat(pArea->OffsetY),
		pArea->Area.cx, pArea->Area.cy);
	return S_OK;
}

// Handle certain known special cases.
HRESULT SpecialCaseAttributeValue(GUID guid, const PROPVARIANT& var)
{
	if ((guid == MF_MT_FRAME_RATE) || (guid == MF_MT_FRAME_RATE_RANGE_MAX) ||
		(guid == MF_MT_FRAME_RATE_RANGE_MIN) || (guid == MF_MT_FRAME_SIZE) ||
		(guid == MF_MT_PIXEL_ASPECT_RATIO))
	{
		// Attributes that contain two packed 32-bit values.
		LogUINT32AsUINT64(var);
	}
	else if ((guid == MF_MT_GEOMETRIC_APERTURE) ||
		(guid == MF_MT_MINIMUM_DISPLAY_APERTURE) ||
		(guid == MF_MT_PAN_SCAN_APERTURE))
	{
		// Attributes that an MFVideoArea structure.
		return LogVideoArea(var);
	}
	else
	{
		return S_FALSE;
	}
	return S_OK;
}

void DBGMSG(PCWSTR format, ...)
{
	va_list args;
	va_start(args, format);

	WCHAR msg[MAX_PATH];

	if (SUCCEEDED(StringCbVPrintfW(msg, sizeof(msg), format, args)))
	{
		//OutputDebugStringW(msg);
		printf("%ls\n", msg);
	}
}

#ifndef IF_EQUAL_RETURN
#define IF_EQUAL_RETURN(param, val) if(val == param) return L#val
#endif

LPCWSTR GetGUIDNameConst(const GUID& guid)
{
	IF_EQUAL_RETURN(guid, MF_MT_MAJOR_TYPE);
	IF_EQUAL_RETURN(guid, MF_MT_MAJOR_TYPE);
	IF_EQUAL_RETURN(guid, MF_MT_SUBTYPE);
	IF_EQUAL_RETURN(guid, MF_MT_ALL_SAMPLES_INDEPENDENT);
	IF_EQUAL_RETURN(guid, MF_MT_FIXED_SIZE_SAMPLES);
	IF_EQUAL_RETURN(guid, MF_MT_COMPRESSED);
	IF_EQUAL_RETURN(guid, MF_MT_SAMPLE_SIZE);
	IF_EQUAL_RETURN(guid, MF_MT_WRAPPED_TYPE);
	IF_EQUAL_RETURN(guid, MF_MT_AUDIO_NUM_CHANNELS);
	IF_EQUAL_RETURN(guid, MF_MT_AUDIO_SAMPLES_PER_SECOND);
	IF_EQUAL_RETURN(guid, MF_MT_AUDIO_FLOAT_SAMPLES_PER_SECOND);
	IF_EQUAL_RETURN(guid, MF_MT_AUDIO_AVG_BYTES_PER_SECOND);
	IF_EQUAL_RETURN(guid, MF_MT_AUDIO_BLOCK_ALIGNMENT);
	IF_EQUAL_RETURN(guid, MF_MT_AUDIO_BITS_PER_SAMPLE);
	IF_EQUAL_RETURN(guid, MF_MT_AUDIO_VALID_BITS_PER_SAMPLE);
	IF_EQUAL_RETURN(guid, MF_MT_AUDIO_SAMPLES_PER_BLOCK);
	IF_EQUAL_RETURN(guid, MF_MT_AUDIO_CHANNEL_MASK);
	IF_EQUAL_RETURN(guid, MF_MT_AUDIO_FOLDDOWN_MATRIX);
	IF_EQUAL_RETURN(guid, MF_MT_AUDIO_WMADRC_PEAKREF);
	IF_EQUAL_RETURN(guid, MF_MT_AUDIO_WMADRC_PEAKTARGET);
	IF_EQUAL_RETURN(guid, MF_MT_AUDIO_WMADRC_AVGREF);
	IF_EQUAL_RETURN(guid, MF_MT_AUDIO_WMADRC_AVGTARGET);
	IF_EQUAL_RETURN(guid, MF_MT_AUDIO_PREFER_WAVEFORMATEX);
	IF_EQUAL_RETURN(guid, MF_MT_AAC_PAYLOAD_TYPE);
	IF_EQUAL_RETURN(guid, MF_MT_AAC_AUDIO_PROFILE_LEVEL_INDICATION);
	IF_EQUAL_RETURN(guid, MF_MT_FRAME_SIZE);
	IF_EQUAL_RETURN(guid, MF_MT_FRAME_RATE);
	IF_EQUAL_RETURN(guid, MF_MT_FRAME_RATE_RANGE_MAX);
	IF_EQUAL_RETURN(guid, MF_MT_FRAME_RATE_RANGE_MIN);
	IF_EQUAL_RETURN(guid, MF_MT_PIXEL_ASPECT_RATIO);
	IF_EQUAL_RETURN(guid, MF_MT_DRM_FLAGS);
	IF_EQUAL_RETURN(guid, MF_MT_PAD_CONTROL_FLAGS);
	IF_EQUAL_RETURN(guid, MF_MT_SOURCE_CONTENT_HINT);
	IF_EQUAL_RETURN(guid, MF_MT_VIDEO_CHROMA_SITING);
	IF_EQUAL_RETURN(guid, MF_MT_INTERLACE_MODE);
	IF_EQUAL_RETURN(guid, MF_MT_TRANSFER_FUNCTION);
	IF_EQUAL_RETURN(guid, MF_MT_VIDEO_PRIMARIES);
	IF_EQUAL_RETURN(guid, MF_MT_CUSTOM_VIDEO_PRIMARIES);
	IF_EQUAL_RETURN(guid, MF_MT_YUV_MATRIX);
	IF_EQUAL_RETURN(guid, MF_MT_VIDEO_LIGHTING);
	IF_EQUAL_RETURN(guid, MF_MT_VIDEO_NOMINAL_RANGE);
	IF_EQUAL_RETURN(guid, MF_MT_GEOMETRIC_APERTURE);
	IF_EQUAL_RETURN(guid, MF_MT_MINIMUM_DISPLAY_APERTURE);
	IF_EQUAL_RETURN(guid, MF_MT_PAN_SCAN_APERTURE);
	IF_EQUAL_RETURN(guid, MF_MT_PAN_SCAN_ENABLED);
	IF_EQUAL_RETURN(guid, MF_MT_AVG_BITRATE);
	IF_EQUAL_RETURN(guid, MF_MT_AVG_BIT_ERROR_RATE);
	IF_EQUAL_RETURN(guid, MF_MT_MAX_KEYFRAME_SPACING);
	IF_EQUAL_RETURN(guid, MF_MT_DEFAULT_STRIDE);
	IF_EQUAL_RETURN(guid, MF_MT_PALETTE);
	IF_EQUAL_RETURN(guid, MF_MT_USER_DATA);
	IF_EQUAL_RETURN(guid, MF_MT_AM_FORMAT_TYPE);
	IF_EQUAL_RETURN(guid, MF_MT_MPEG_START_TIME_CODE);
	IF_EQUAL_RETURN(guid, MF_MT_MPEG2_PROFILE);
	IF_EQUAL_RETURN(guid, MF_MT_MPEG2_LEVEL);
	IF_EQUAL_RETURN(guid, MF_MT_MPEG2_FLAGS);
	IF_EQUAL_RETURN(guid, MF_MT_MPEG_SEQUENCE_HEADER);
	IF_EQUAL_RETURN(guid, MF_MT_DV_AAUX_SRC_PACK_0);
	IF_EQUAL_RETURN(guid, MF_MT_DV_AAUX_CTRL_PACK_0);
	IF_EQUAL_RETURN(guid, MF_MT_DV_AAUX_SRC_PACK_1);
	IF_EQUAL_RETURN(guid, MF_MT_DV_AAUX_CTRL_PACK_1);
	IF_EQUAL_RETURN(guid, MF_MT_DV_VAUX_SRC_PACK);
	IF_EQUAL_RETURN(guid, MF_MT_DV_VAUX_CTRL_PACK);
	IF_EQUAL_RETURN(guid, MF_MT_ARBITRARY_HEADER);
	IF_EQUAL_RETURN(guid, MF_MT_ARBITRARY_FORMAT);
	IF_EQUAL_RETURN(guid, MF_MT_IMAGE_LOSS_TOLERANT);
	IF_EQUAL_RETURN(guid, MF_MT_MPEG4_SAMPLE_DESCRIPTION);
	IF_EQUAL_RETURN(guid, MF_MT_MPEG4_CURRENT_SAMPLE_ENTRY);
	IF_EQUAL_RETURN(guid, MF_MT_ORIGINAL_4CC);
	IF_EQUAL_RETURN(guid, MF_MT_ORIGINAL_WAVE_FORMAT_TAG);

	// Media types

	IF_EQUAL_RETURN(guid, MFMediaType_Audio);
	IF_EQUAL_RETURN(guid, MFMediaType_Video);
	IF_EQUAL_RETURN(guid, MFMediaType_Protected);
	IF_EQUAL_RETURN(guid, MFMediaType_SAMI);
	IF_EQUAL_RETURN(guid, MFMediaType_Script);
	IF_EQUAL_RETURN(guid, MFMediaType_Image);
	IF_EQUAL_RETURN(guid, MFMediaType_HTML);
	IF_EQUAL_RETURN(guid, MFMediaType_Binary);
	IF_EQUAL_RETURN(guid, MFMediaType_FileTransfer);

	IF_EQUAL_RETURN(guid, MFVideoFormat_AI44); //     FCC('AI44')
	IF_EQUAL_RETURN(guid, MFVideoFormat_ARGB32); //   D3DFMT_A8R8G8B8 
	IF_EQUAL_RETURN(guid, MFVideoFormat_AYUV); //     FCC('AYUV')
	IF_EQUAL_RETURN(guid, MFVideoFormat_DV25); //     FCC('dv25')
	IF_EQUAL_RETURN(guid, MFVideoFormat_DV50); //     FCC('dv50')
	IF_EQUAL_RETURN(guid, MFVideoFormat_DVH1); //     FCC('dvh1')
	IF_EQUAL_RETURN(guid, MFVideoFormat_DVSD); //     FCC('dvsd')
	IF_EQUAL_RETURN(guid, MFVideoFormat_DVSL); //     FCC('dvsl')
	IF_EQUAL_RETURN(guid, MFVideoFormat_H264); //     FCC('H264')
	IF_EQUAL_RETURN(guid, MFVideoFormat_I420); //     FCC('I420')
	IF_EQUAL_RETURN(guid, MFVideoFormat_IYUV); //     FCC('IYUV')
	IF_EQUAL_RETURN(guid, MFVideoFormat_M4S2); //     FCC('M4S2')
	IF_EQUAL_RETURN(guid, MFVideoFormat_MJPG);
	IF_EQUAL_RETURN(guid, MFVideoFormat_MP43); //     FCC('MP43')
	IF_EQUAL_RETURN(guid, MFVideoFormat_MP4S); //     FCC('MP4S')
	IF_EQUAL_RETURN(guid, MFVideoFormat_MP4V); //     FCC('MP4V')
	IF_EQUAL_RETURN(guid, MFVideoFormat_MPG1); //     FCC('MPG1')
	IF_EQUAL_RETURN(guid, MFVideoFormat_MSS1); //     FCC('MSS1')
	IF_EQUAL_RETURN(guid, MFVideoFormat_MSS2); //     FCC('MSS2')
	IF_EQUAL_RETURN(guid, MFVideoFormat_NV11); //     FCC('NV11')
	IF_EQUAL_RETURN(guid, MFVideoFormat_NV12); //     FCC('NV12')
	IF_EQUAL_RETURN(guid, MFVideoFormat_P010); //     FCC('P010')
	IF_EQUAL_RETURN(guid, MFVideoFormat_P016); //     FCC('P016')
	IF_EQUAL_RETURN(guid, MFVideoFormat_P210); //     FCC('P210')
	IF_EQUAL_RETURN(guid, MFVideoFormat_P216); //     FCC('P216')
	IF_EQUAL_RETURN(guid, MFVideoFormat_RGB24); //    D3DFMT_R8G8B8 
	IF_EQUAL_RETURN(guid, MFVideoFormat_RGB32); //    D3DFMT_X8R8G8B8 
	IF_EQUAL_RETURN(guid, MFVideoFormat_RGB555); //   D3DFMT_X1R5G5B5 
	IF_EQUAL_RETURN(guid, MFVideoFormat_RGB565); //   D3DFMT_R5G6B5 
	IF_EQUAL_RETURN(guid, MFVideoFormat_RGB8);
	IF_EQUAL_RETURN(guid, MFVideoFormat_UYVY); //     FCC('UYVY')
	IF_EQUAL_RETURN(guid, MFVideoFormat_v210); //     FCC('v210')
	IF_EQUAL_RETURN(guid, MFVideoFormat_v410); //     FCC('v410')
	IF_EQUAL_RETURN(guid, MFVideoFormat_WMV1); //     FCC('WMV1')
	IF_EQUAL_RETURN(guid, MFVideoFormat_WMV2); //     FCC('WMV2')
	IF_EQUAL_RETURN(guid, MFVideoFormat_WMV3); //     FCC('WMV3')
	IF_EQUAL_RETURN(guid, MFVideoFormat_WVC1); //     FCC('WVC1')
	IF_EQUAL_RETURN(guid, MFVideoFormat_Y210); //     FCC('Y210')
	IF_EQUAL_RETURN(guid, MFVideoFormat_Y216); //     FCC('Y216')
	IF_EQUAL_RETURN(guid, MFVideoFormat_Y410); //     FCC('Y410')
	IF_EQUAL_RETURN(guid, MFVideoFormat_Y416); //     FCC('Y416')
	IF_EQUAL_RETURN(guid, MFVideoFormat_Y41P);
	IF_EQUAL_RETURN(guid, MFVideoFormat_Y41T);
	IF_EQUAL_RETURN(guid, MFVideoFormat_YUY2); //     FCC('YUY2')
	IF_EQUAL_RETURN(guid, MFVideoFormat_YV12); //     FCC('YV12')
	IF_EQUAL_RETURN(guid, MFVideoFormat_YVYU);

	IF_EQUAL_RETURN(guid, MFAudioFormat_PCM); //              WAVE_FORMAT_PCM 
	IF_EQUAL_RETURN(guid, MFAudioFormat_Float); //            WAVE_FORMAT_IEEE_FLOAT 
	IF_EQUAL_RETURN(guid, MFAudioFormat_DTS); //              WAVE_FORMAT_DTS 
	IF_EQUAL_RETURN(guid, MFAudioFormat_Dolby_AC3_SPDIF); //  WAVE_FORMAT_DOLBY_AC3_SPDIF 
	IF_EQUAL_RETURN(guid, MFAudioFormat_DRM); //              WAVE_FORMAT_DRM 
	IF_EQUAL_RETURN(guid, MFAudioFormat_WMAudioV8); //        WAVE_FORMAT_WMAUDIO2 
	IF_EQUAL_RETURN(guid, MFAudioFormat_WMAudioV9); //        WAVE_FORMAT_WMAUDIO3 
	IF_EQUAL_RETURN(guid, MFAudioFormat_WMAudio_Lossless); // WAVE_FORMAT_WMAUDIO_LOSSLESS 
	IF_EQUAL_RETURN(guid, MFAudioFormat_WMASPDIF); //         WAVE_FORMAT_WMASPDIF 
	IF_EQUAL_RETURN(guid, MFAudioFormat_MSP1); //             WAVE_FORMAT_WMAVOICE9 
	IF_EQUAL_RETURN(guid, MFAudioFormat_MP3); //              WAVE_FORMAT_MPEGLAYER3 
	IF_EQUAL_RETURN(guid, MFAudioFormat_MPEG); //             WAVE_FORMAT_MPEG 
	IF_EQUAL_RETURN(guid, MFAudioFormat_AAC); //              WAVE_FORMAT_MPEG_HEAAC 
	IF_EQUAL_RETURN(guid, MFAudioFormat_ADTS); //             WAVE_FORMAT_MPEG_ADTS_AAC 

	return NULL;
}


MFcamera::MFcamera()
{
	//CoInitialize(NULL);
	m_bConnected		= false;
	m_nWidth				= 0;
	m_nHeight				= 0;
	m_bLock					= false;
	m_bChanged			= false;
	m_nBufferSize			= 0;
	m_nFlipMode			= 0;

	MFStartup(MF_VERSION);

	m_pDevices				= NULL;
	m_pSource				= NULL;
	m_pAttributes			= NULL;
	m_pType					= NULL;
	m_pReader				= NULL;
	m_pwszSymbolicLink=new WCHAR;
	m_pBuffer				= NULL;
	m_cchSymbolicLink	= 0;
	//InitializeCriticalSection(&m_critsec);
	//CoUninitialize();
}

//TODO
MFcamera::~MFcamera()
{
	//CoInitialize(NULL);
	if (m_pDevices != NULL)
		CloseCamera();
	//DeleteCriticalSection(&m_critsec);
	MFShutdown();
	//CoUninitialize();
}

int MFcamera::CameraCount()
{

	//CoInitialize(NULL);

	HRESULT hr = S_OK;
	MFDeviceParam param = { 0 };
	IMFAttributes *pAttributes = NULL;

	// Initialize an attribute store to specify enumeration parameters.

	hr = MFCreateAttributes(&pAttributes, 1);

	if (FAILED(hr)){
		SafeRelease(&pAttributes);
		//CoUninitialize();
		return -1;
	}

	// Ask for source type = video capture devices.
	hr = pAttributes->SetGUID(
		MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE,
		MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE_VIDCAP_GUID
	);

	if (FAILED(hr)) {
		SafeRelease(&pAttributes);
		//CoUninitialize();
		return -1;
	}

	//Enumerate devices
	UINT32 tempNum = 0;
	hr = MFEnumDeviceSources(pAttributes, &param.ppDevices, &tempNum);

	if (FAILED(hr)) {
		SafeRelease(&pAttributes);

		for (int i = 0; i < param.count; i++)
		{
			SafeRelease(&param.ppDevices[i]);
		}
		CoTaskMemFree(param.ppDevices);
		//CoUninitialize();
		return -1;
	}

	// NOTE: param.count might be zero.
	param.count = tempNum;
	if (param.count < 0)
	{
		param.count = 0;
	}


	SafeRelease(&pAttributes);

	for (int i = 0; i < param.count; i++)
	{
		SafeRelease(&param.ppDevices[i]);
	}
	CoTaskMemFree(param.ppDevices);
	//CoUninitialize();
	return param.count;
}

bool MFcamera::GetCameraIndexByFriendlyName(int& nCamID, const char* sName, int nBufferSize)
{

	//CoInitialize(NULL);
	HRESULT hr = S_OK;
	MFDeviceParam param = { 0 };
	param.ppDevices = NULL;
	IMFAttributes *pAttributes = NULL;

	bool isFound = false;

	// Initialize an attribute store to specify enumeration parameters.

	hr = MFCreateAttributes(&pAttributes, 1);


	// Ask for source type = video capture devices.
	hr = pAttributes->SetGUID(
		MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE,
		MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE_VIDCAP_GUID
	);

	//Enumerate devices
	UINT32 tempNum = 0;
	MFEnumDeviceSources(pAttributes, &param.ppDevices, &tempNum);

	// NOTE: param.count might be zero.
	param.count = (int)tempNum;
	if (param.count < 0)
	{
		param.count = 0;

		SafeRelease(&pAttributes);

		for (int i = 0; i < param.count; i++)
		{
			SafeRelease(&param.ppDevices[i]);
		}

		//CoUninitialize();
		return isFound;
	}

	//Compare Name to get Index
	for (int i = 0; i < param.count; i++)
	{
		WCHAR* szFriendllyName = NULL;
		UINT32 szNameSize = 0;
		hr = param.ppDevices[i]->GetAllocatedString(
			MF_DEVSOURCE_ATTRIBUTE_FRIENDLY_NAME,
			&szFriendllyName,
			&szNameSize
		);

		if (SUCCEEDED(hr))
		{
			printf("Find name %ls\n", szFriendllyName);
			char* tempchar = (char *)CoTaskMemAlloc(szNameSize + 1);//new char(szNameSize+1);
			memset(tempchar, 0, szNameSize + 1);
			WideCharToMultiByte(CP_ACP, 0, szFriendllyName, -1, tempchar, szNameSize, "", NULL);
			printf("Find name %s\n", tempchar);
			if (!memcmp((void*)tempchar, (void*)sName, nBufferSize<szNameSize ? nBufferSize : szNameSize ))
			{
				nCamID = i;
				isFound = true;
			}
			CoTaskMemFree(tempchar);
		}
		//CoTaskMemFree(szFriendllyName);
		if (isFound)
			break;
	}

	SafeRelease(&pAttributes);

	for (int i = 0; i < param.count; i++)
	{
		SafeRelease(&param.ppDevices[i]);
		//param.ppDevices[i]->Release();
	}

	//CoUninitialize();
	return isFound;

}

//bool MFcamera::CameraDescription(int nCamID, char* sName, int nBufferSize)
//{
//	return true;
//	//int count = 0;
//	////CoInitialize(NULL);
//
//	//CComPtr<ICreateDevEnum> pCreateDevEnum;
//	//HRESULT hr = CoCreateInstance(CLSID_SystemDeviceEnum, NULL, CLSCTX_INPROC_SERVER,
//	//	IID_ICreateDevEnum, (void**)&pCreateDevEnum);
//
//	//CComPtr<IEnumMoniker> pEm;
//	//hr = pCreateDevEnum->CreateClassEnumerator(CLSID_VideoInputDeviceCategory,
//	//	&pEm, 0);
//	//if (hr != NOERROR) return 0;
//
//
//	//pEm->Reset();
//	//ULONG cFetched;
//	//IMoniker *pM;
//	//while (hr = pEm->Next(1, &pM, &cFetched), hr == S_OK)
//	//{
//	//	if (count == nCamID)
//	//	{
//	//		IPropertyBag *pBag = 0;
//	//		hr = pM->BindToStorage(0, 0, IID_IPropertyBag, (void **)&pBag);
//	//		if (SUCCEEDED(hr))
//	//		{
//	//			VARIANT var;
//	//			var.vt = VT_BSTR;
//	//			hr = pBag->Read(L"Description", &var, NULL); //还有其他属性,像描述信息等等...
//	//			if (hr == NOERROR)
//	//			{
//	//				//获取设备名称			
//	//				WideCharToMultiByte(CP_ACP, 0, var.bstrVal, -1, sName, nBufferSize, "", NULL);
//
//	//				SysFreeString(var.bstrVal);
//	//			}
//	//			pBag->Release();
//	//		}
//	//		pM->Release();
//
//	//		break;
//	//	}
//	//	count++;
//	//}
//
//	//pCreateDevEnum = NULL;
//	//pEm = NULL;
//
//	//return 1;
//	
//}

//bool MFcamera::CameraDevicePath(int nCamID, char* sName, int nBufferSize)
//{
//	return true;
//	//int count = 0;
//	////CoInitialize(NULL);
//
//	//CComPtr<ICreateDevEnum> pCreateDevEnum;
//	//HRESULT hr = CoCreateInstance(CLSID_SystemDeviceEnum, NULL, CLSCTX_INPROC_SERVER,
//	//	IID_ICreateDevEnum, (void**)&pCreateDevEnum);
//
//	//CComPtr<IEnumMoniker> pEm;
//	//hr = pCreateDevEnum->CreateClassEnumerator(CLSID_VideoInputDeviceCategory,
//	//	&pEm, 0);
//	//if (hr != NOERROR) return 0;
//
//
//	//pEm->Reset();
//	//ULONG cFetched;
//	//IMoniker *pM;
//	//while (hr = pEm->Next(1, &pM, &cFetched), hr == S_OK)
//	//{
//	//	if (count == nCamID)
//	//	{
//	//		IPropertyBag *pBag = 0;
//	//		hr = pM->BindToStorage(0, 0, IID_IPropertyBag, (void **)&pBag);
//	//		if (SUCCEEDED(hr))
//	//		{
//	//			VARIANT var;
//	//			var.vt = VT_BSTR;
//	//			hr = pBag->Read(L"DevicePath", &var, NULL); //还有其他属性,像描述信息等等...
//	//			if (hr == NOERROR)
//	//			{
//	//				//获取设备名称			
//	//				WideCharToMultiByte(CP_ACP, 0, var.bstrVal, -1, sName, nBufferSize, "", NULL);
//
//	//				SysFreeString(var.bstrVal);
//	//			}
//	//			pBag->Release();
//	//		}
//	//		pM->Release();
//
//	//		break;
//	//	}
//	//	count++;
//	//}
//
//	//pCreateDevEnum = NULL;
//	//pEm = NULL;
//
//	//return 1;
//}

bool MFcamera::OpenCamera(int nCamID,
	int nWidth,
	int nHeight,
	int nFrameRate,
	int nFlip) {


	//CoInitialize(NULL);
	HRESULT hr = S_OK;
	MFDeviceParam param = { 0 };
	param.ppDevices = NULL;
	
	IMFAttributes *pAttributes = NULL;
	IMFMediaType *pType = NULL;
	IMFPresentationDescriptor *pPD = NULL;
	IMFStreamDescriptor *pSD, *pVideoStreamDescription = NULL;
	IMFMediaTypeHandler *pHandler = NULL;

	// Initialize an attribute store to specify enumeration parameters.

	hr = MFCreateAttributes(&pAttributes, 1);

	// Ask for source type = video capture devices.
	hr = pAttributes->SetGUID(
		MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE,
		MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE_VIDCAP_GUID
	);


	//Enumerate devices
	UINT32 tempNum = 0;
	MFEnumDeviceSources(pAttributes, &param.ppDevices, &tempNum);

	// NOTE: param.count might be zero.
	param.count = (int)tempNum;
	if (param.count < 0 || nCamID>param.count)
	{
		param.count = 0;

		SafeRelease(&pAttributes);

		for (int i = 0; i < param.count; i++)
		{
			SafeRelease(&param.ppDevices[i]);
		}
		CoTaskMemFree(param.ppDevices);
		//CoUninitialize();
		return false;
	}

	m_pDevices = param.ppDevices[nCamID];

	SafeRelease(&pAttributes);
	pAttributes = NULL;

	//EnterCriticalSection(&m_critsec);

	// Create the media source for the device.
	MFCreateDeviceSource(pAttributes, &m_pSource);

	m_pDevices->ActivateObject(
            __uuidof(IMFMediaSource), 
            (void**)&m_pSource
            );

	printf(" Create the media source OK\n");
	// Get the symbolic link.
	hr = m_pDevices->GetAllocatedString(
		MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE_VIDCAP_SYMBOLIC_LINK,
		&m_pwszSymbolicLink,
		&m_cchSymbolicLink
	);

	printf("Get the symbolic link. OK\n");
	//Create Source Reader
	{
		if (SUCCEEDED(hr)) {
			hr = MFCreateAttributes(&pAttributes, 2);
		}

		if (SUCCEEDED(hr))
		{
			hr = pAttributes->SetUINT32(MF_READWRITE_DISABLE_CONVERTERS, TRUE);
		}

		if (SUCCEEDED(hr))
		{
			hr = MFCreateSourceReaderFromMediaSource(
				m_pSource,
				pAttributes,
				&m_pReader
			);
		}
	}

	printf("Create Source Reader OK\n");
	//Get and Set Video Capture Formats
	{

		BOOL bSelected=false;
		bool bFindVideoStream=false;
		DWORD count = 0;
		DWORD MediaTypecount = 0;
		GUID majoytype = GUID_NULL;
		GUID subtype = GUID_NULL;
		//Find Select Video Stream
		hr = m_pSource->CreatePresentationDescriptor(&pPD);
		//Media描述符下包含了自己所有流的描述符信息，可以通过getcount来获取该Media的流数目，通过Index获取流描述符
		if (SUCCEEDED(hr))
		{
			hr = pPD->GetStreamDescriptorCount(&count);
			printf("Have %d Stream DescriptorCount\n",count);
		}

		//GUID majorType, subtype;
		if (SUCCEEDED(hr))
		{
			for (int i = 0; i < count; i++)
			{
				DWORD StreamNum = 0;
				hr = pPD->GetStreamDescriptorByIndex(i, &bSelected, &pSD);

				if (FAILED(hr))
				{
					break;
				}
				if (bSelected)
					{
						hr = pSD->GetMediaTypeHandler(&pHandler);
						if(SUCCEEDED(hr))
							hr = pHandler->GetCurrentMediaType(&pType);
						if (SUCCEEDED(hr))
							hr = pType->GetGUID(MF_MT_MAJOR_TYPE, &majoytype);
						if (SUCCEEDED(hr))
						{
							if (majoytype == MFMediaType_Video)
							{
								bFindVideoStream = true;
								break;
							}
						}
					}
				
			}
		}

		if (FAILED(hr) || !bFindVideoStream)
		{
			SafeRelease(&pPD);
			SafeRelease(&pSD);
			SafeRelease(&pHandler);
			SafeRelease(&pType);
			SafeRelease(&m_pReader);
			SafeRelease(&pAttributes);
			SafeRelease(&m_pDevices);
			SafeRelease(&m_pSource);
			for (int i = 0; i < param.count; i++)
			{
				SafeRelease(&param.ppDevices[i]);
			}
			CoTaskMemFree(param.ppDevices);
			//LeaveCriticalSection(&m_critsec);
			//CoUninitialize();

			return false;
		}

		bool bisFindSupportType = false;
		//Get all Formats
		{
			DWORD typecount = 0;
			pHandler->GetMediaTypeCount(&typecount);
			printf("Total Fomate: %d", typecount);

			for (int i = 0; i < typecount; i++)
			{
				pHandler->GetMediaTypeByIndex(i, &pType);
				//subject
				{
					pType->GetGUID(MF_MT_SUBTYPE, &subtype);
					if (subtype == MFVideoFormat_RGB24)
						m_colorspace = RGB24;
					else if (subtype == MFVideoFormat_RGB32)
						m_colorspace = RGB32;
					else if (subtype == MFVideoFormat_YUY2)
						m_colorspace = YUY2;
					else if (subtype == MFVideoFormat_RGB8)
						m_colorspace = GARY;
					else if (subtype == MFVideoFormat_RGB555)
						m_colorspace = RGB555;
					else if (subtype == MFVideoFormat_A2R10G10B10)
						m_colorspace = RGB10;
					else if (subtype == MFVideoFormat_P010)
						m_colorspace = YUV10;
					else if (subtype == MFVideoFormat_MJPG)
						m_colorspace = MJPG;
					else if (subtype == MFVideoFormat_NV12)
						m_colorspace = NV12;
					else
						m_colorspace = OTHER;
				}

				//width and heigh
				MFGetAttributeSize(pType, MF_MT_FRAME_SIZE, &m_nWidth, &m_nHeight);

				//Frame Rate
				{
					UINT32 frame_rate_num, frame_rate_denominator;
					hr = MFGetAttributeRatio(pType, MF_MT_FRAME_RATE, &frame_rate_num, &frame_rate_denominator);
					if (SUCCEEDED(hr))
					{
						m_nFrameRate = frame_rate_num / frame_rate_denominator;
					}
				}

				printf("----------------------------------------------------------------------------------------\n");
				//LogMediaType(pType);
				//OutputDebugString("\n");
				printf("Index %d: Current set color_space %d\n width %d height %d\n framerate %d\n", i, m_colorspace, m_nWidth, m_nHeight, m_nFrameRate);

				if (m_colorspace==RGB24&& m_nWidth == nWidth&&m_nHeight == nHeight&&m_nFrameRate == nFrameRate)
				{
					hr = pHandler->SetCurrentMediaType(pType);
					bisFindSupportType = true;
					break;
				}

			}
		}
		//Set Media Type
		{
				hr = pHandler->GetCurrentMediaType(&pType);

				hr = pHandler->GetCurrentMediaType(&m_pType);
				//subject
				{
					pType->GetGUID(MF_MT_SUBTYPE, &subtype);
					if (subtype == MFVideoFormat_RGB24)
						m_colorspace = RGB24;
					else if (subtype == MFVideoFormat_RGB32)
						m_colorspace = RGB32;
					else if (subtype == MFVideoFormat_YUY2)
						m_colorspace = YUY2;
					else if (subtype == MFVideoFormat_RGB8)
						m_colorspace = GARY;
					else if (subtype == MFVideoFormat_RGB555)
						m_colorspace = RGB555;
					else if (subtype == MFVideoFormat_A2R10G10B10)
						m_colorspace = RGB10;
					else if (subtype == MFVideoFormat_P010)
						m_colorspace = YUV10;
					else if (subtype == MFVideoFormat_MJPG)
						m_colorspace = MJPG;
					else if (subtype == MFVideoFormat_NV12)
						m_colorspace = NV12;
					else
						m_colorspace = OTHER;
				}

				//width and heigh
				MFGetAttributeSize(pType, MF_MT_FRAME_SIZE, &m_nWidth, &m_nHeight);

				//Frame Rate
				{
					UINT32 frame_rate_num, frame_rate_denominator;
					hr = MFGetAttributeRatio(pType, MF_MT_FRAME_RATE, &frame_rate_num, &frame_rate_denominator);
					if (SUCCEEDED(hr))
					{
						m_nFrameRate = frame_rate_num / frame_rate_denominator;
					}

					printf("Current set color_space %d\n width %d height %d\n framerate %d\n", m_colorspace, m_nWidth, m_nHeight, m_nFrameRate);
					//}
				}

				if (!bisFindSupportType)
				{
					SafeRelease(&pPD);
					SafeRelease(&pSD);
					SafeRelease(&pHandler);
					SafeRelease(&pType);
					SafeRelease(&pAttributes);
					//LeaveCriticalSection(&m_critsec);
					//CoUninitialize();
					return false;
				}
		}
		//Set Source Reader Media Type
		//{
		//	IMFMediaType *pNativeType = NULL;
		//	if (pType != NULL)
		//	{
		//		pType->Release();
		//		pType = NULL;
		//	}
		//	HRESULT hr = m_pReader->GetNativeMediaType(0, 0, &pNativeType);
		//	LogMediaType(pNativeType);

		//	hr = MFCreateMediaType(&pType);
		//	pType->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Video);
		//	pType->SetGUID(MF_MT_SUBTYPE, MFVideoFormat_MJPG);
		//	hr=m_pReader->SetCurrentMediaType(0, NULL, pType);
		//	if (SUCCEEDED(hr))
		//	{
		//		printf("Save Reader OK\n");
		//	}

		//	hr = m_pReader->GetNativeMediaType(0, 0, &pNativeType);
		//	LogMediaType(pNativeType);

		//	SafeRelease(&pNativeType);

		//}




		//	if (FAILED(hr))
		//	{
		//		SafeRelease(&pSetType);
		//		SafeRelease(&pSet_ResultType);
		//		SafeRelease(&pPD);
		//		SafeRelease(&pSD);
		//		SafeRelease(&pHandler);
		//		SafeRelease(&pType);
		//		SafeRelease(&m_pReader);
		//		SafeRelease(&pAttributes);
		//		SafeRelease(&m_pDevices);
		//		SafeRelease(&m_pSource);
		//		for (int i = 0; i < param.count; i++)
		//		{
		//			SafeRelease(&param.ppDevices[i]);
		//		}
		//		CoTaskMemFree(param.ppDevices);
		//		LeaveCriticalSection(&m_critsec);

		//		//CoUninitialize();


		//		printf("Set failed \n");
		//		return false;
		//	}
		//}
	}



	SafeRelease(&pPD);
	SafeRelease(&pSD);
	SafeRelease(&pHandler);
	SafeRelease(&pType);
	SafeRelease(&pAttributes);


	FindFlipMFT();


	//LeaveCriticalSection(&m_critsec);
	//CoUninitialize();
	return true;
}

void MFcamera::CloseCamera() {
	HRESULT hr = S_OK;
	//CoInitialize(NULL);
	//EnterCriticalSection(&m_critsec);
	if (m_pReader != NULL)
		hr = CloseDevice();

	if (SUCCEEDED(hr))
	{
		if (m_pSource)
		{
			m_pSource->Shutdown();
		}
		SafeRelease(&m_pDevices);
		SafeRelease(&m_pSource);
		SafeRelease(&m_pAttributes);
	}

	//LeaveCriticalSection(&m_critsec);
}

HRESULT MFcamera::CloseDevice()
{
	//EnterCriticalSection(&m_critsec);

	SafeRelease(&m_pReader);
	m_pReader = NULL;
	CoTaskMemFree(m_pwszSymbolicLink);
	m_pwszSymbolicLink = NULL;
	m_cchSymbolicLink = 0;

	//LeaveCriticalSection(&m_critsec);
	return S_OK;
}

bool MFcamera::QueryFrame(UINT8* pdata,LONGLONG *pLLtimestamp)
{

	HRESULT hr = S_OK;
	//CoInitialize(NULL);
	IMFSample* pSample=NULL;
	bool quit = false;
	size_t  cSamples = 0;
	IMFMediaBuffer * pBuffle = NULL;

	UINT8* pData;


	DWORD streamIndex, flags;
	LONGLONG llTimeStamp;

	//EnterCriticalSection(&m_critsec);

	if (m_pReader == NULL)
	{

		//LeaveCriticalSection(&m_critsec);

		printf("fail intput1\n");
		return false;
	}
	
		hr = m_pReader->ReadSample(
			MF_SOURCE_READER_FIRST_VIDEO_STREAM,    // Stream index.
			0,                              // Flags.
			&streamIndex,                   // Receives the actual stream index. 
			&flags,                         // Receives status flags.
			&llTimeStamp,                   // Receives the time stamp.
			&pSample                        // Receives the sample or NULL.
		);


		if (pSample)
		{
			++cSamples;





			m_pFilpMFT->ProcessMessage(MFT_MESSAGE_NOTIFY_BEGIN_STREAMING, 0);

			hr=m_pFilpMFT->ProcessInput(0, pSample, 0);
			while (hr == MF_E_NOTACCEPTING)
			{
				hr = m_pFilpMFT->ProcessMessage(MFT_MESSAGE_COMMAND_FLUSH, 0);
				if (SUCCEEDED(hr))
				{
					hr = m_pFilpMFT->ProcessInput(0, pSample, 0);
					printf("input\n");
				}
			}



			MFT_OUTPUT_DATA_BUFFER tempoutpt;
			tempoutpt.dwStreamID = 0;
			IMFSample* poutputSample = NULL;
			IMFMediaBuffer * poutputBuffer = NULL;
			hr = MFCreateSample(&poutputSample);
			if (SUCCEEDED(hr))
			{
				hr = MFCreateMemoryBuffer(640 * 480 * 3, &poutputBuffer);
				printf("sample OK\n");
			}
			if (SUCCEEDED(hr))
			{
				hr = poutputSample->AddBuffer(poutputBuffer);
				printf("buffer ok\n");
			}
			if (SUCCEEDED(hr))
			{
				printf("add buffer OK\n");
			}

			tempoutpt.pSample = poutputSample; 
			tempoutpt.dwStatus = 0;
			tempoutpt.pEvents = NULL;
			DWORD tempstatus;

			hr=m_pFilpMFT->ProcessOutput(0, 1, &tempoutpt, &tempstatus);

			if (SUCCEEDED(hr))
			{
			DWORD maxlengh ;
			DWORD curlenth;
			
			hr = poutputSample->GetBufferByIndex(0, &pBuffle);

			DWORD cBuffers = 0;

			

			pBuffle->Lock(&pData, &maxlengh, &curlenth);

			FlipImage(pData, pdata);
				
			pBuffle->Unlock();
			pSample->RemoveAllBuffers();
			pSample->Release();
			pBuffle->Release();
			}
			else
			{
				printf("%d\n", hr);
			}


			m_pFilpMFT->ProcessMessage(MFT_MESSAGE_NOTIFY_END_OF_STREAM, 0);
			m_pFilpMFT->ProcessMessage(MFT_MESSAGE_COMMAND_DRAIN, 0);


		    poutputSample->RemoveAllBuffers();
			poutputSample = NULL;
		}

		//LeaveCriticalSection(&m_critsec);
	//SafeRelease(&pSample);
	//SafeRelease(&pBuffle);

	return true;
}

bool MFcamera::FlipImage(UINT8 * pSrc, UINT8* pDst)
{

	switch (m_nFlipMode)
	{
	case 0: {
		memcpy(pDst, pSrc, m_nWidth * m_nHeight * 3);
		break;
	}
	case 1: {
		//Hor
		for (int i = 0; i < m_nHeight; i++)
		{
			for (int j = 0; j < m_nWidth; j++)
			{
				UINT32 thisIndex = m_nWidth * i + j;
				UINT32 changeIndex = m_nWidth * i + (m_nWidth - 1) - j;
				*(pDst + 3 * (thisIndex)) = *(pSrc + 3 * (changeIndex));
				*(pDst + 3 * (thisIndex)+1) = *(pSrc + 3 * (changeIndex)+1);
				*(pDst + 3 * (thisIndex)+2) = *(pSrc + 3 * (changeIndex)+2);
			}
		}
		break;
	}
	case 2: {
		//Hor
		for (int i = 0; i < m_nHeight; i++)
		{
			for (int j = 0; j < m_nWidth; j++)
			{
				UINT32 thisIndex = m_nWidth * i + j;
				UINT32 changeIndex = m_nWidth * (m_nHeight - 1 - i) + j;
				*(pDst + 3 * (thisIndex)) = *(pSrc + 3 * (changeIndex));
				*(pDst + 3 * (thisIndex)+1) = *(pSrc + 3 * (changeIndex)+1);
				*(pDst + 3 * (thisIndex)+2) = *(pSrc + 3 * (changeIndex)+2);
			}
		}
		break;
	}
	case 3: {
		//Hor
		for (int i = 0; i < m_nHeight; i++)
		{
			for (int j = 0; j < m_nWidth; j++)
			{
				UINT32 thisIndex = m_nWidth * i + j;
				UINT32 changeIndex = m_nWidth * (m_nHeight - 1 - i) + (m_nWidth - 1) - j;
				*(pDst + 3 * (thisIndex)) = *(pSrc + 3 * (changeIndex));
				*(pDst + 3 * (thisIndex)+1) = *(pSrc + 3 * (changeIndex)+1);
				*(pDst + 3 * (thisIndex)+2) = *(pSrc + 3 * (changeIndex)+2);
			}
		}
		break;
	}
	default:
		break;
	}
	return true;
}


bool MFcamera::FindFlipMFT()
{
	//CoInitialize(NULL);
	if (m_pFlipProcessor != NULL)
		return false;

	HRESULT hr = CoCreateInstance(CLSID_VideoProcessorMFT, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&m_pFilpMFT));
	if (SUCCEEDED(hr))
		printf("Build succeed\n");


	hr = m_pFilpMFT->QueryInterface(IID_PPV_ARGS(&m_pFlipProcessor));

	if (SUCCEEDED(hr))
	{
		printf("Query succeed\n");

		hr = m_pFlipProcessor->SetMirror(MIRROR_NONE);
		if (SUCCEEDED(hr))
		{
			printf("Set flip ok\n");
		}

		m_pFlipProcessor->SetRotation(ROTATION_NORMAL);
	}



	DWORD inputStreamNum, outputStreamNum;

	hr = m_pFilpMFT->GetStreamCount(&inputStreamNum, &outputStreamNum);
	if (SUCCEEDED(hr))
		printf("MFT input count %d output count %d\n", inputStreamNum, outputStreamNum);

	IMFMediaType *ptempinputmediatype = NULL;
	MFCreateMediaType(&ptempinputmediatype);
	//for (int i = 0; i < 20; i++) {
	//	hr = m_pFilpMFT->GetOutputAvailableType(0, 0, &ptempinputmediatype);
	//	if (SUCCEEDED(hr))
	//		LogMediaType(ptempinputmediatype);
	//	GUID tempsubject;
	//	ptempinputmediatype->GetGUID(MF_MT_SUBTYPE, &tempsubject);
	//		if (tempsubject == MFVideoFormat_RGB24)
	//			break;
	//}

	//ptempinputmediatype->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Video);
	//ptempinputmediatype->SetGUID(MF_MT_SUBTYPE, MFVideoFormat_RGB24);


	//HRESULT hrsetinput = m_pFilpMFT->SetOutputType(0, ptempinputmediatype, 0);
	//

	//if (!SUCCEEDED(hrsetinput))
	//{
	//	printf("%d", hrsetinput);
	//}

	//ptempinputmediatype = NULL;

	//MFCreateMediaType(&ptempinputmediatype);

	//hr = m_pFilpMFT->GetOutputCurrentType(0, &ptempinputmediatype);

	//LogMediaType(ptempinputmediatype);

	for (int i = 0; i < 20; i++) {
		hr=m_pFilpMFT->GetInputAvailableType(0, i, &ptempinputmediatype);
		if (SUCCEEDED(hr)) {
			GUID tempsubject;
			ptempinputmediatype->GetGUID(MF_MT_SUBTYPE, &tempsubject);
			if (tempsubject == MFVideoFormat_RGB24)
			{
				printf("Find available type\n");
				break;
			}
		}
		else
		{
			printf("%d\n", hr);
		}
	}

	 hr = m_pFilpMFT->SetInputType(0, m_pType, 0);

	 if (SUCCEEDED(hr))
		 printf("Set input OK\n");

	 hr = m_pFilpMFT->SetOutputType(0, m_pType, 0);
	 if (SUCCEEDED(hr))
		 printf("Set output OK\n");


	return true;
}


bool MFcamera::GetMFTOutPut(IMFTransform *outputMFT, IMFSample *psample)
{
	MFT_OUTPUT_STREAM_INFO mftStreamInfo = { 0 };
	outputMFT->GetOutputStreamInfo(0, &mftStreamInfo);

	MFT_OUTPUT_DATA_BUFFER tempoutpt;
	tempoutpt.dwStreamID = 0;
	IMFMediaBuffer * poutputBuffer = NULL;
	HRESULT hr = MFCreateSample(&psample);


	if (SUCCEEDED(hr))
	{
		hr = MFCreateMemoryBuffer(mftStreamInfo.cbSize, &poutputBuffer);
		printf("sample OK\n");
	}
	if (SUCCEEDED(hr))
	{
		hr = psample->AddBuffer(poutputBuffer);
		printf("buffer ok\n");
	}
	if (SUCCEEDED(hr))
	{
		printf("add buffer OK\n");
	}

	tempoutpt.pSample = psample;
	tempoutpt.dwStatus = 0;
	tempoutpt.pEvents = NULL;
	DWORD tempstatus;

	hr = m_pFilpMFT->ProcessOutput(0, 1, &tempoutpt, &tempstatus);

	return SUCCEEDED(hr);

}