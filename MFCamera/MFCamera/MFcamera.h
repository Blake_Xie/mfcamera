#pragma once

#include <mfapi.h>
#include <mfidl.h>
#include <mfreadwrite.h>
#include <mferror.h>
#include <mftransform.h>

#include <stdio.h>


template <class T> void SafeRelease(T **ppT)
{
	if (*ppT!=NULL)
	{
		(*ppT)->Release();
		*ppT = NULL;
	}
}


class MFcamera
{
public:
	unsigned char* m_pTemp;
private:
	bool m_bConnected;
	UINT32 m_nWidth;
	UINT32 m_nHeight;
	UINT32 m_nFrameRate;

	enum ColorSpace
	{
		RGB24=0,
		RGB32=1,
		YUY2=2,
		GARY=3,
		RGB555=4,
		RGB10=5,
		YUV10=6,
		MJPG =7,
		NV12=8,
		OTHER=255

	} m_colorspace;

	bool m_bLock;
	bool m_bChanged;
	long m_nBufferSize;


	struct MFDeviceParam
	{
		IMFActivate **ppDevices;
		int count=0;
	};

	IMFActivate				*m_pDevices ;
	IMFMediaSource		*m_pSource	;
	IMFAttributes			*m_pAttributes ;
	IMFMediaType		*m_pType ;
	IMFSourceReader	*m_pReader;
	IMFMediaBuffer		*m_pBuffer;
	IMFTransform			*m_pFilpMFT;
	IMFTransform			*m_pColorSpaceMFT;
	IMFVideoProcessorControl		*m_pFlipProcessor;


	WCHAR                   *m_pwszSymbolicLink;
	UINT32                     m_cchSymbolicLink;


	//the last byte means the FlipHorizontal
	//the last 2 byte means the FlipVertical
	unsigned char m_nFlipMode;

	//CRITICAL_SECTION        m_critsec;

public:
	MFcamera();
	~MFcamera();

	//返回摄像头的数目
	//可以不用创建MFcamera实例，采用int c=MFcamera::CameraCount();得到结果。
	static int CameraCount();

	//根据摄像头的名字返回摄像头编号
	//nCamID: 摄像头编号
	//sName: 用于存放摄像头名字的数组
	//nBufferSize: sName的大小，大小为strlen(sName)
	//可以不用创建MFcamera实例，采用MFcamera::CameraName();得到结果。
	static bool GetCameraIndexByFriendlyName(int& nCamID, const char* sName, int nBufferSize);

	////根据摄像头的编号返回摄像头的描述符
	////nCamID: 摄像头编号
	////sName: 用于存放摄像头描述符的数组
	////nBufferSize: sName的大小
	////可以不用创建MFcamera实例，采用MFcamera::CameraDescription();得到结果。
	//static bool CameraDescription(int nCamID, char* sName, int nBufferSize);

	////根据摄像头的编号返回摄像头的设备路径
	////nCamID: 摄像头编号
	////sName: 用于存放摄像头名字的数组
	////nBufferSize: sName的大小
	////可以不用创建MFcameraDS实例，采用MFcamera::CameraDevicePath();得到结果。
	//static bool CameraDevicePath(int nCamID, char* sName, int nBufferSize);

	//打开摄像头，nCamID指定打开哪个摄像头，取值可以为0,1,2,...
	//nWidth和nHeight设置的摄像头的宽和高，如果摄像头不支持所设定的宽度和高度，则返回false
	bool OpenCamera(int nCamID,
		int nWidth = 640,
		int nHeight = 480,
		int nFrameRate = 30,
		int nFlip = 0);

	//关闭摄像头，析构函数会自动调用这个函数
	void CloseCamera();

	bool QueryFrame(UINT8* pdata,  LONGLONG *pLLtimestamp);

private:

	HRESULT CloseDevice();
	HRESULT TryMediaType();
	bool FlipImage(UINT8 * pSrc,UINT8* pDst);
	bool FindFlipMFT();

	bool GetMFTOutPut(IMFTransform *outputMFT, IMFSample *psample);

};