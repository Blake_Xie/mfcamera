#include "MFcamera.h"
#include <stdio.h>
#include <core.hpp>
#include <highgui.hpp>

using namespace cv;

int main()
{
	MFcamera a;
	int index = 100;
	const char* failname = "luanqibazao";
	const char* rightname = "Logitech HD Pro Webcam C920";

	printf("Count :  %d\n", MFcamera::CameraCount());


	if (MFcamera::GetCameraIndexByFriendlyName(index, rightname, strlen(rightname)))
	{
		printf("Right, the camera Index is %d\n",index);
	}
	else
	{
		printf("Error, Can't find the right name camera\n");
	}


//	printf("Open? %d\n",
	a.OpenCamera(index);

	
	Mat test(cvSize(640, 480), CV_8UC3);
	LONGLONG stamp = 0;
	bool con = true;
	
	
	while (con)
	{
		a.QueryFrame(test.data, &stamp);
		if (!test.empty())
			imshow("test", test);

		switch (cvWaitKey(10))
		{
		case 'a': {con = false; break; }
		case 'b': {a.CloseCamera(); break; }
		case 'c': {a.OpenCamera(0, 640, 480); }
		default:
			break;
		}
	}
	destroyAllWindows();


	a.CloseCamera();
	getchar();
	return 1;
}